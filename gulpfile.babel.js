import gulp from 'gulp'
import path from 'path'
import pug from 'gulp-pug'
import plumber from 'gulp-plumber'
import stylus from 'gulp-stylus'
import imagemin from 'gulp-imagemin'
import concat from 'gulp-concat'
import autoprefixer from 'gulp-autoprefixer'
import server from 'gulp-develop-server'
import util from 'gulp-util'
import webpackStream from 'webpack-stream'
import webpack from 'webpack'

import options from './webpack.config.babel.js'

/* @gulp: default */
gulp.task('default',['dist', 'watch'], () => {
	server.listen({path: './server.js',execArgv: ['--harmony']}, () => {
		util.log(util.colors.red.bold(':: DEBUG MODE ::'))
	});
})

/* @gulp: dist*/
gulp.task('dist', ['js', 'assets', 'assetsImg', 'assetsCatalogs', 'assetsArticles', 'assetsVideos', 'stylus'], () => {
	return gulp.src('./src/template/index.pug')
	.pipe(pug())
	.pipe(gulp.dest('dist'))
})

/* @gulp: js */
gulp.task('js', callback => {
	return gulp.src('./src/js/index.js')
	.pipe(plumber())
	.pipe(webpackStream(options, webpack))
	.pipe(gulp.dest('dist/js'))
	.on('data', () => {
		if (!callback.called) {
			callback.called = true;
			callback();
		}
	});
})

/* @gulp: assets */
gulp.task('assets', () => {
    return gulp.src('src/assets/fonts/**/*')
        .pipe(gulp.dest('dist/fonts/'))
})

gulp.task('assetsVideos', () => {
    return gulp.src('src/assets/videos/**/*')
        .pipe(gulp.dest('dist/videos/'))
})

gulp.task('assetsCatalogs', () => {
    return gulp.src('src/assets/catalogs/**/*')
        .pipe(gulp.dest('dist/catalogs/'))
})

gulp.task('assetsArticles', () => {
    return gulp.src('src/assets/articles/**/*')
        .pipe(gulp.dest('dist/articles/'))
})

/* @gulp: assets */
gulp.task('assetsImg', () => {
	return gulp.src('src/assets/img/**/*')
		// .pipe(imagemin())
		.pipe(gulp.dest('dist/img/'))
})

/* @gulp: stylus */
gulp.task('stylus', function() {
    return gulp.src('src/stylus/index.styl')
        .pipe(plumber())
        .pipe(stylus())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('dist/css'))
})

/* @gulp: watch */
gulp.task('watch', () => {
	gulp.watch('src/template/index.pug', ['dist'])
	gulp.watch('src/assets/fonts/**/*', ['assets'])
	gulp.watch('src/assets/img/**/*', ['assetsImg'])
	gulp.watch('src/assets/catalogs/**/*', ['assetsCatalogs'])
	gulp.watch('src/assets/articles/**/*', ['assetsArticles'])
	gulp.watch('src/assets/videos/**/*', ['assetsVideos'])
	gulp.watch('src/**/*.styl', ['stylus'])
})