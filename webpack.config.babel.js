import webpack from 'webpack'
import path from 'path'

const dev = process.env.NODE_ENV !== 'production';

const options = {
	devtool: dev ? 'source-map-eval' : false,
    watch: dev,

    output: {
        path: path.resolve(__dirname, 'dist/js'),
        publicPath: '/js/',
        filename: dev ? '[name].js' : '[chunkhash:12.bundle].js'
    },
    module: {
        rules: [{
            test: /\.vue$/,
            loader: 'vue-loader'
        },{
            test: /\.js?$/,
            exclude: [/node_modules/, /dist/],
            use: [{
            	loader: 'babel-loader',
            	options: {
                    presets: ['es2015', 'stage-2'],
                }
            }]
        },{
            test: /\.styl$/,
            exclude: [/node_modules/, /dist/],
            use:[
                 "style-loader",
                 "css-loader",
                 "stylus-loader"
            ]
        },{
            test: /\.pug?$/,
            exclude: [/node_modules/, /dist/],
            loader: ['raw-loader', 'pug-html-loader']
        }]
    },
    resolve: {
        modules: [
            path.join(__dirname, 'src'),
            'node_modules'
        ],
        extensions: ['.js', '.pug', '.styl'],
        alias: {
          'vue$': 'vue/dist/vue.js',
          'vue-router$': 'vue-router/dist/vue-router.common.js',
          'img': path.resolve('./src/assets/img/')
        }
    },

    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            'window.jQuery': "jquery"
        })
    ]
}

export default options