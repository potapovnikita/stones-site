import template from '../../template/components/contacts.pug'

export default {
    template,
    props: ['ContactsData', 'selectedLanguage'],
    mounted() {
        this.initYaMap()
    },
    methods: {
        initYaMap() {
            ymaps.ready(this.createMap);
        },
        createMap() {
            const myMap = new ymaps.Map("map", {
                center: [56.83624642, 60.58627316],
                zoom: 16
            });

            const hintContent = this.selectedLanguage.type === "ru" ? this.ContactsData.addressRu : this.ContactsData.addressEng
            const iconCaption = this.selectedLanguage.type === "ru" ? this.ContactsData.addressRu : this.ContactsData.addressEng

            const myPlacemark = new ymaps.Placemark([56.83624642, 60.58627316], {
                hintContent: hintContent,
                iconCaption: iconCaption
            })

            myMap.geoObjects.add(myPlacemark);
        }
    }
}