import template from '../../template/components/services.pug'
import FullImg from "./img-full";
import * as emailjs from "emailjs-com/dist/email";

export default {
    template,
    components: {
        FullImg
    },
    props: ['ServicesData', 'selectedLanguage'],
    data() {
        return {
            showService: false,
            imgFull: false,
            images: [],
            initial: 0,
            pageName: '',
            contactName: '',
            contactPhone: '',
            contactEmail: '',
            contactComment: '',
            emailStatus: '',
            emailStatusEng: '',
            errorName: false,
            errorPhone: false,
        }
    },
    methods: {
        openService(service) {
            this.images = service.images
            this.pageName = service
            this.showService = true
        },
        openImg(service, index) {
            this.imgFull = true
            this.initial = index
        },
        closeShowService() {
            this.pageName = ''
            this.showService = false
            this.images = []
        },
        closeFullScreen() {
            this.imgFull = false
            this.initial = 0
        },
        sendMail() {
            const emailParams = {
                name: this.contactName,
                phone: this.contactPhone,
                email: this.contactEmail,
                comment: this.contactComment,
                service: this.pageName.name
            };

            if (!this.contactName || !this.contactPhone) {
                this.errorName = !this.contactName
                this.errorPhone = !this.contactPhone
            } else {
                this.errorName = false
                this.errorPhone = false
                emailjs.send("mail_ru", "stone-crafting", emailParams)
                    .then((res) => {
                        this.emailStatus = 'Заявка отправлена, мы скоро с Вами свяжемся'
                        this.emailStatusEng = 'Your application has been sent, we will contact you shortly.'
                        this.contactName = ''
                        this.contactPhone = ''
                        this.contactEmail = ''
                        this.contactComment = ''
                    }, (error) => {
                        this.emailStatus = 'Что-то пошло не так, попробуйте позже или свяжитесь с нами по телефону'
                        this.emailStatusEng = 'Something went wrong, try again later or contact us by phone'
                    });
            }
        }
    },
    mounted() {
        emailjs.init('user_0qIsrdRv9hVhOoGLjqibi')
    },
    watch: {
        contactName(val) {
            if (val) this.errorName = false
            else this.errorName = true
        },
        contactPhone(val) {
            if (val) this.errorPhone = false
            else this.errorPhone = true
        },
        emailStatus(val) {
            if (val) {
                this.errorName = false
                this.errorPhone = false
            }
        }
    }
}