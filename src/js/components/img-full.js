import template from '../../template/components/img-full.pug'

export default {
    template,
    props: ['images', 'initial', 'closeFullScreen'],
    mounted() {
        $('.full-img-container_slick').slick({
            initialSlide: this.initial,
            infinite: true,
            dots: true,
            speed: 500,
            arrows: true,
            cssEase: 'linear',
            pauseOnHover: false,
            pauseOnDotsHover: true,
        });
    }
}