import template from '../../template/components/stone-art.pug'
import FullImg from './img-full'

export default {
    props: ['DataRu', 'DataEng', 'selectedLanguage'],
    data() {
        return {
            type: this.$route.params.name,
            isShowInfo: false,
            Data: this.selectedLanguage.type === 'ru' ? this.DataRu : this.DataEng,
            infoData: {},
            imgFull: false,
            images: [],
            initial: 0,
            index: null,
            currentScroll: 0
        }
    },
    template,
    components: {
        FullImg
    },
    methods: {
        selectItem(item, index) {
            this.infoData = item
            this.isShowInfo = true
            this.index = index
        },
        openImg(images, index) {
            this.imgFull = true
            this.images = images
            this.initial = index
        },
        closeFullScreen() {
            this.imgFull = false
            this.images = []
            this.initial = 0
        },
        back() {
            this.isShowInfo = !this.isShowInfo
            setTimeout(() => {
                window.scrollTo(0, this.currentScroll)
            })
        }
    },
    created() {
        addEventListener("scroll", () => {
            var html = document.documentElement;
            var body = document.body;
            var scrollTop = html.scrollTop || body && body.scrollTop || 0;

            scrollTop -= html.clientTop
            if (!this.isShowInfo) {
                this.currentScroll = scrollTop
            }
        })
    },
    computed: {
        pagesLength() {
            return this.Data.pages[this.type].pageItems.length !== 1
        }
    },
    watch: {
        '$route'() {
            this.type = this.$route.params.name
            this.isShowInfo = false
            this.Data = this.selectedLanguage.type === 'ru' ? this.DataRu : this.DataEng

        },
        selectedLanguage(newVal) {
            if (newVal.type === 'ru') {
                this.Data = this.DataRu
            } else  this.Data = this.DataEng
            if (this.isShowInfo) {
                this.infoData = this.Data.pages[this.type].pageItems[this.index]
            }
        },

    }
}