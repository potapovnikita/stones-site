import template from '../../template/components/slider.pug'

export default {
    template,
    mounted() {
        $('.single-item').slick({
            infinite: true,
            dots: true,
            autoplay: true,
            autoplaySpeed: 3000,
            speed: 500,
            arrows: true,
            cssEase: 'linear',
            pauseOnHover: false,
            pauseOnDotsHover: true,
        });

        $('.single-item').slick('slickPause')
    },
}