import template from '../../template/components/films.pug'

export default {
    template,
    props: ['selectedLanguage', 'Data'],
    methods: {
        goBack() {
            this.$router.go(-1)
        }
    }
}