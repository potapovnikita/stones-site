import template from '../../template/components/smi.pug'

export default {
    template,
    props: ['selectedLanguage', 'Data'],
    methods: {
        goBack() {
            this.$router.go(-1)
        }
    }
}