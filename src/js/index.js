import Vue from 'vue'
import VueRouter from 'vue-router'
import MenuData from '../data/menu.json'
import StoneArtDataRu from '../data/stone-art.json'
import StoneArtDataEng from '../data/stone-art-eng.json'
import CollectionsData from '../data/collections.json'
import CollectionsDataEng from '../data/collections-eng.json'
import ServicesData from '../data/services.json'
import CompanyData from '../data/company.json'
import ContactsData from '../data/contacts.json'
import ShowsData from '../data/shows.json'
import ClickOutside from 'vue-click-outside'
import template from '../template/App.pug'
import 'slick-carousel'

import Main from './components/slider' // главная
import About from './components/about' //   О компании
import AboutMain from './components/aboutMain' //   О компании
import StoneArt from './components/stone-art' // камнерезное искусство
import Services from './components/services' // услуги
import Show from './components/show' // выставки
import Contacts from './components/contacts' // контакты
import Films from './components/films' // фильмы
import Smi from './components/smi' // сми



Vue.use(VueRouter)

const routes = [
    { path: '/company', component: About, props: { Data: CompanyData},
        children: [
            { path:'', component: AboutMain},
            { path:'films', component: Films},
            { path:'smi', component: Smi}
        ] },
    { path: '/stone-art/:name', component: StoneArt, props: { DataRu: StoneArtDataRu,  DataEng: StoneArtDataEng} },
    { path: '/collections/:name', component: StoneArt, props: { DataRu: CollectionsData,  DataEng: CollectionsDataEng} },
    { path: '/services', component: Services, props: { ServicesData } },
    { path: '/show', component: Show, props: { ShowsData }},
    { path: '/contacts', component: Contacts, props: { ContactsData } },
    { path: '/', component: Main}
]

const router = new VueRouter({
    mode: 'history',
    routes
})


const app = new Vue({
    el: '#app',
    template,
    router,
    data() {
        return {
            menu: MenuData,
            contacts: ContactsData,
            menuOpen: false,
            preview: true,
            previewHide: true,
            showSelectLanguage: false,
            languages: [
                {
                    type: 'ru',
                    name: 'Русский'
                },{
                    type: 'en',
                    name: 'English'
                }
            ],
            menuStoneArt: false,
            menuCollections: false,
            selectedLanguage: null
        }
    },
    methods: {
        hideMenu() {
            this.menuOpen = false
            this.menuStoneArt = false
            this.menuCollections = false
        },
        hideSelectLanguage() {
            this.showSelectLanguage = false
        },
        setLanguage(option) {
            this.selectedLanguage = option
            this.showSelectLanguage = false
        },
        showSubMenu(type) {
            switch (type) {
                case 'stoneArt':
                    this.menuStoneArt = true
                    return
                case 'collections':
                    this.menuCollections = true
                    return


            }
        },
        hideSubMenu(type) {
            switch (type) {
                case 'stoneArt':
                    this.menuStoneArt = false
                case 'collections':
                    this.menuCollections = false

            }
        }
    },
    created() {
        this.selectedLanguage = this.languages[0]

        this.popupItem = this.$el
        setTimeout(() => {
            this.preview = false
        }, 2500)
        setTimeout(() => {
            this.previewHide = false
            $('.single-item').slick('slickPlay')
        }, 4000)
    },
    directives: {
        ClickOutside
    },
    watch: {
        menuOpen(newVal) {
            if(newVal) $('.single-item').slick('slickPause')
            else $('.single-item').slick('slickPlay')
        }
    }
});


(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter49240795 = new Ya.Metrika2({
                id:49240795,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true
            });
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/tag.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks2");

